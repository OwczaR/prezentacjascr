\beamer@endinputifotherversion {3.24pt}
\beamer@sectionintoc {1}{Szyfrowanie}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{Poj\IeC {\k e}cia podstawowe}{4}{0}{1}
\beamer@subsectionintoc {1}{2}{Szyfry klasyczne}{8}{0}{1}
\beamer@subsectionintoc {1}{3}{Szyfrowanie symetryczne i asymetryczne}{12}{0}{1}
\beamer@subsectionintoc {1}{4}{Przyk\IeC {\l }ady obecnie stosowanych algorytm\IeC {\'o}w szyfrowania}{16}{0}{1}
\beamer@subsectionintoc {1}{5}{Przyk\IeC {\l }adowe zastosowania kryptografii}{23}{0}{1}
\beamer@sectionintoc {2}{Dystrybucja Kluczy}{26}{0}{2}
\beamer@subsectionintoc {2}{1}{Certyfikaty}{27}{0}{2}
\beamer@subsectionintoc {2}{2}{PKI}{30}{0}{2}
